create database TestingSystem;
use TestingSystem;

create table Department (
	DepartmentID int,
    DepartmentName varchar(50)
);

create table Position (
	PositionID int,
    PositionName varchar(50)
);

create table `Account` (
	AccountID int,
    Email varchar(50),
    Usename varchar(50),
    Fullname varchar(50),
    DepartmentID int,
    PositionID int,
    CreateDate date
);

create table `Group` (
	GroupID int,
    GroupName varchar(50),
    CreatorID int,
    CreateDate date
);

create table GroupAccount (
	GroupID int,
    AccountID int,
    JoinDate date
);

create table TypeQuestion (
	TypeID int,
    TypeName varchar(50)
);

create table CategoryQuestion (
	CategoryID int,
    CategoryName varchar(50)
);

create table Question (
	QuestionID int,
    Content varchar(50),
    CategoryID int,
    CreatorID int,
    CreateDate date
);

create table Answer (
	AnswerID int,
    Content varchar(50),
    QuestionID int,
    isCorrect bool
);

create table Exam(
	ExamID int,
    `Code` int,
    Title varchar(50),
    Duration date,
    CreatorID int,
    CreateDate date
);

create table ExamQuestion (
	ExamID int,
    QuestionID int
);
